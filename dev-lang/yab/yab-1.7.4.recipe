SUMMARY="Extended version of the yabsic programming language"
DESCRIPTION="
yab is an extended version of yabasic, a BASIC programming language, with \
special commands designed for Haiku.
"
HOMEPAGE="https://github.com/bbjimmy/Yab"
SRC_URI="https://github.com/bbjimmy/Yab/archive/yab1.7.4.tar.gz"
CHECKSUM_SHA256="c757e7382006e7c266082c75d4aeefa951ed84a1ad64e97180e886cdda7f859a"

REVISION="1"
LICENSE="Artistic"
COPYRIGHT="
	1995-2006 Marc-Oliver Ihm (yabasic)
	2006-2009 Jan Bungeroth (yab improvements)
	2013 Jim Saxton (yab improvements)
	"

ARCHITECTURES="x86_gcc2 x86 ?x86_64"

PROVIDES="
	yab = $portVersion compat >= 1.7
	cmd:yab= $portVersion compat >= 1.7
	"
REQUIRES="
	haiku$secondaryArchSuffix
	lib:libncurses$secondaryArchSuffix
	"
BUILD_REQUIRES="
	haiku${secondaryArchSuffix}_devel
	devel:libncurses$secondaryArchSuffix
	"
BUILD_PREREQUIRES="
	cmd:bison
	cmd:flex
	cmd:gcc$secondaryArchSuffix
	cmd:make
	cmd:mkdepend
	cmd:unzip
	"

SOURCE_DIR="Yab-yab1.7.4"

BUILD()
{	
	mkdir -p tmp
	cp -r src/* tmp
	cd tmp
	make $jobArgs BUILDHOME=`finddir B_SYSTEM_DEVELOP_DIRECTORY`
	unzip -o App_YAB.zip
	copyattr App_YAB yab
}

INSTALL()
{	
	mkdir -p $binDir
	cp tmp/yab  $binDir/
	
	mkdir -p $appsDir/yab-IDE
	cp -r src $appsDir/yab-IDE/src
	cp -r Documentation $appsDir/yab-IDE/Documentation

	mkdir -p $documentationDir
	ln -s $appsDir/yab-IDE/Documentation $documentationDir/yab-1.7.4
}
